import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.SecretKey;

public class Q3 {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		SecretKey sk = (SecretKey) readFromFile("data/secretKey1");
		String encodedHmac = (String) readFromFile("data/hmac");
		byte[] sentHmac = Base64.getDecoder().decode(encodedHmac);
		
		String sentText = (String) readFromFile("data/sendText1");
		
		System.out.println(sentHmac.length);
		System.out.println("Base64 encoded message digest: "+ encodedHmac);
		
		//calculate hmac
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(sk);
		byte[] myHmac = mac.doFinal(sentText.getBytes());
		
		//check hmac
		System.out.println("Check: "+Arrays.equals(sentHmac, myHmac));
		
		

	}
	
	static Object readFromFile(String filename) throws Exception {
		FileInputStream fin = new FileInputStream(filename);
		ObjectInputStream oin = new ObjectInputStream(fin);
		Object object = oin.readObject();
		oin.close();
		return object;
		}

}
