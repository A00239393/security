import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

public class Q2 {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		String message = "This is a sample message";
		//write secret key
		KeyGenerator kg = KeyGenerator.getInstance("HmacSHA256");
		SecretKey sk = kg.generateKey();
		writeToFile("data/secretKey1", sk);
		writeToFile("data/sendText1", message);
		
		//write hmac
		byte[] textArray = message.getBytes();
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(sk);
		byte[] hmac = mac.doFinal(textArray);
		String encodedHmac = Base64.getEncoder().encodeToString(hmac);
		writeToFile("data/hmac",encodedHmac);
		
		System.out.println(hmac.length);
		System.out.println("Base64 encoded message digest: "+ encodedHmac);
		
		
		

	    
	}

	private static void writeToFile(String filename, Object object) throws IOException {
		// TODO Auto-generated method stub
		
		FileOutputStream fout = new FileOutputStream(filename);
		ObjectOutputStream oout = new ObjectOutputStream(fout);
		oout.writeObject(object);
		oout.close();
		
		
	}
	
	static Object readFromFile(String filename) throws Exception {
		FileInputStream fin = new FileInputStream(filename);
		ObjectInputStream oin = new ObjectInputStream(fin);
		Object object = oin.readObject();
		oin.close();
		return object;
		}

	
	


}
