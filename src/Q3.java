import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.apache.commons.codec.digest.DigestUtils;

public class Q3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String sessionid = "12345";
		String md5 = DigestUtils.md5Hex(sessionid);
		System.out.println("sessionid " + sessionid +
		" md5 version is " + md5);
		String sha256 = DigestUtils.sha256Hex(sessionid);
		System.out.println("sessionid " + sessionid +
		" sha256 version is " + sha256);
	}

}
