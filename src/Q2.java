import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Q2 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		FileInputStream fin = new FileInputStream("data/test");

		///////////
		MessageDigest algorithm= null;
		try {
			algorithm = MessageDigest.getInstance("SHA-256");
			
		}
		catch(NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		

		byte[] buffer = new byte[32];
		int bytesRead = 0;
		
		while((bytesRead = fin.read(buffer)) > 0)
		{
			algorithm.update(buffer, 0 , bytesRead);
			System.out.print(bytesRead);
		}
		byte[] messageDigest2 = algorithm.digest();
		System.out.println("length " + messageDigest2.length);

		String encodedDigest2 = Base64.getEncoder().encodeToString(messageDigest2);;
		System.out.println("Base64 encoded message digest " + encodedDigest2);
	}

}
