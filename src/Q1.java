import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;


public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		//
		String password1= "aaaaa";
		String password2= "aaaa";
		
		MessageDigest algorithm= null;
		try {
			algorithm = MessageDigest.getInstance("SHA-256");
			
		}
		catch(NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		
		algorithm.reset();
		algorithm.update(password1.getBytes());
		byte[] messageDigest1 = algorithm.digest();
		
		System.out.println("length " + messageDigest1.length);

		String encodedDigest = Base64.getEncoder().encodeToString(messageDigest1);;
		System.out.println("Base64 encoded message digest " + encodedDigest);
		
		
		//can calculate message digest with digest() for a single string
		byte[] messageDigest2 = algorithm.digest(password2.getBytes());
		System.out.println("length " + messageDigest2.length);

		String encodedDigest2 = Base64.getEncoder().encodeToString(messageDigest2);;
		System.out.println("Base64 encoded message digest " + encodedDigest2);
		
		

	}

}
