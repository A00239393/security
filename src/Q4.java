import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;

import org.apache.commons.codec.digest.DigestUtils;

public class Q4 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		FileInputStream fin = new FileInputStream("data/test");
		byte[] sha256 = DigestUtils.sha256(fin);
		
		String encodedDigest2 = Base64.getEncoder().encodeToString(sha256);;
		System.out.println("Base64 encoded message digest " + encodedDigest2);
	}

}
