import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;

public class Q3 {

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, IOException, ClassNotFoundException, BadPaddingException {
		// TODO Auto-generated method stub

		// encrypt

		String ALGORITHM = "Blowfish";
		KeyGenerator keygen = KeyGenerator.getInstance(ALGORITHM);
		SecretKey key = keygen.generateKey();
		Cipher eCipher = Cipher.getInstance(ALGORITHM);
		// Initialise the cipher for encryption
		eCipher.init(Cipher.ENCRYPT_MODE, key);

		// sealed

		SealedObject so = new SealedObject(new Employee("Mark", "Mayo", "1234"), eCipher);
		System.out.println(so);

/////////////////////////////////////////////////
//Decrypt

		Cipher dCipher = Cipher.getInstance(ALGORITHM);
		dCipher.init(Cipher.DECRYPT_MODE, key);
		
		//get object
		
		Employee o = (Employee) so.getObject(dCipher);
		System.out.println("Employee Name: " + o.getName());

	}

}
