import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;

public class Q4 {

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, IOException, ClassNotFoundException, BadPaddingException {
		// TODO Auto-generated method stub

		// encrypt

		String ALGORITHM = "Blowfish";
		KeyGenerator keygen = KeyGenerator.getInstance(ALGORITHM);
		SecretKey key = keygen.generateKey();
		Cipher eCipher = Cipher.getInstance(ALGORITHM);
		// Initialise the cipher for encryption
		eCipher.init(Cipher.ENCRYPT_MODE, key);

		// sealed

		SealedObject so = new SealedObject(new Employee("Mark", "Mayo", "1234"), eCipher);
		System.out.println(so);
		writeToFile("data/sealFile", so);
		writeToFile("data/cipherFile", eCipher);


	}
	
	private static void writeToFile(String filename, Object object) throws IOException {
		// TODO Auto-generated method stub
		
		FileOutputStream fout = new FileOutputStream(filename);
		ObjectOutputStream oout = new ObjectOutputStream(fout);
		oout.writeObject(object);
		oout.close();
		
		
	}
	

}
