import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class Q2 {

	public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		// TODO Auto-generated method stub

		String ALGORITHM = "Blowfish";
		KeyGenerator keygen = KeyGenerator.getInstance(ALGORITHM);
		SecretKey key = keygen.generateKey();
		Cipher eCipher = Cipher.getInstance(ALGORITHM);
		// Initialise the cipher for encryption
		eCipher.init(Cipher.ENCRYPT_MODE, key);
		String s = "This is just an emaple123";
		System.out.println("Clear text: " + s);
		byte[] cleartext = s.getBytes();
		// Encrypt the cleartext
		byte[] ciphertext = eCipher.doFinal(cleartext);
		System.out.println("Cipher text: " + ciphertext);

/////////////////////////////////////////////////
// Decrypt

		Cipher dCipher = Cipher.getInstance(ALGORITHM);
		dCipher.init(Cipher.DECRYPT_MODE, key);
// Decrypt the ciphertext
		byte[] clearText1 = dCipher.doFinal(ciphertext);
		String text = new String(clearText1);
		System.out.println("Decrypted text: " + text);
	}

}
