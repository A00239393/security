import java.io.FileInputStream;
import java.io.ObjectInputStream;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Q5 {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		
		Object so = (Object)readFromFile("data/sealFile");
		String dCihper = (String)readFromFile("data/cipherFile");
		
		
		Cipher dCipher = Cipher.getInstance(ALGORITHM);
		dCipher.init(Cipher.DECRYPT_MODE, key);
		
		//get object
		
		
		Employee o = (Employee) so.getObject(dCipher);
		System.out.println("Employee Name: " + o.getName());

		
	}

	static Object readFromFile(String filename) throws Exception {
		FileInputStream fin = new FileInputStream(filename);
		ObjectInputStream oin = new ObjectInputStream(fin);
		Object object = oin.readObject();
		oin.close();
		return object;
	}
}
